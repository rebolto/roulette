import java.util.Scanner;

public class Roulette {
    private int money = 1000;

    public static void main(String[] args) {
        int numberBet=0;
        char decision;
        int betAmount=0;
        String input;
        String playername;
        int playermoney = 1000;
        Scanner kb = new Scanner(System.in);
        Roulettewheel wheel = new Roulettewheel();
        System.out.println("Welcome to Roulette!");
        System.out.println("What is your Name?");
        playername = kb.nextLine();
        System.out.println("Alright " + playername + ", " + "you have " + playermoney + " dollars!");
        System.out.println(playername + " Would you like to bet on this turn?");
        System.out.println("Yes(y) or No(n)");
        input = kb.nextLine().toLowerCase();
        decision = input.charAt(0);
        
        if (decision == 'y') {
            System.out.println("How much money would you like to bet?");
            betAmount=kb.nextInt();
            while(betAmount>playermoney||betAmount<0){
            System.out.println("Cannot be more or less than the amount of money you have.");
            System.out.println("How much money would you like to bet?");
            betAmount = kb.nextInt();
            }
            if (betAmount <= playermoney && betAmount >= 0) {
                System.out.println(playername + " bets " + betAmount + " dollars.");
                System.out.println("What number would you like to bet on? Ranges from 1-36");
                
                numberBet = kb.nextInt();
                while(numberBet>36||numberBet<=0){
                    System.out.println("Cannot be less than 0 or higher than 36.");
                    System.out.println("What number would you like to bet on? Ranges from 1-36");
                    numberBet = kb.nextInt();
                }
                wheel.spin();
                if (ifWin(numberBet, wheel.getValue()) == true) {
                    playermoney = playermoney + (betAmount * 35);
                    System.out.println(playername + " you now have " + playermoney + "dollars!");
                } else {
                    System.out.println("the winning number was "+wheel.getValue()+". ");
                    playermoney = playermoney - betAmount;
                    if(betAmount==0){
                        System.out.println("Your money stays the same.");
                    }
                    System.out.println("You now have "+playermoney+" dollars");
                }
            }
        } if(decision=='n') {
            
            System.out.println("Alrighty then.we'll see you next time!");
        }
    }

    public static boolean ifWin(int betNumber, int winningNumber) {
        if (betNumber == winningNumber) {
            System.out.println("Congratulations you have won!");
            return true;
        } else {
            System.out.println("Sorry you lost...");

            return false;
        }
    }

}